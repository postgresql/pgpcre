Source: pgpcre
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 libpcre2-dev,
 pkgconf,
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/petere/pgpcre
Vcs-Browser: https://salsa.debian.org/postgresql/pgpcre
Vcs-Git: https://salsa.debian.org/postgresql/pgpcre.git

Package: postgresql-17-pgpcre
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: Perl Compatible Regular Expressions (PCRE) extension for PostgreSQL
 This is a module for PostgreSQL that exposes Perl-compatible regular
 expressions (PCRE) functionality as functions and operators. It is based on
 the popular PCRE library.
 .
 Some possible advantages over the regular expression support built into
 PostgreSQL:
  * richer pattern language, more familiar to Perl and Python programmers
  * complete Unicode support
  * saner operators and functions
 Some disadvantages:
  * no repeated matching ('g' flag)
  * no index optimization
